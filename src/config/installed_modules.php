<?php
return [
    "admin" => [
        "class" => "nullref\\admin\\Module"    
    ],
    "geo" => [
        "class" => "nullref\\geo\\Module"    
    ],
    "category" => [
        "class" => "nullref\\category\\Module"    
    ],
    "cms" => [
        "class" => "nullref\\cms\\Module"    
    ],
    "blog" => [
        "class" => "nullref\\blog\\Module"    
    ]
    ];