<?php

$installed_modules = require(__DIR__ . '/installed_modules.php');
$modules = [
    'core' => ['class' => 'nullref\core\Module'],
    'cms' => [
        'class' => 'nullref\cms\Module',
        'urlPrefix' => '/page/',
        'components' => [
            'blockManager' => [
                'class' => 'app\components\BlockManager',
            ],
        ],
        'classMap' => [
            'Page' => 'app\modules\cms\models\Page',
        ],

    ],
    'admin' => [
        'class' => 'nullref\admin\Module',
        'adminModel' => 'app\models\Admin',
        'components' => [
            'menuBuilder' => 'app\components\MenuBuilder',
        ],
    ],
    'category' => [
        'classMap' => [
            'Category' => 'app\models\Category',
        ],
    ],
    'product' => [
        'class' => 'nullref\product\Module',
        'productModel' => 'app\models\Product',
    ],
];

if (isset($installed_modules['blog'])) {
    $modules['blog'] = [
        'classMap' => [
            'Post' => 'app\models\BlogPost',
            'PostQuery' => 'app\models\BlogPostQuery',
            'BlogStatusList' => 'app\components\BlogStatusList',
        ],
    ];
}

return yii\helpers\ArrayHelper::merge($installed_modules, $modules);