<?php
/** @var $this \yii\web\View */
use app\models\BlogPost;

?>
<?php $data = (new \yii\data\ActiveDataProvider([
    'query' => BlogPost::find(),
]))->getModels() ?>
<?= \nullref\datatable\DataTable::widget([
    'data' => [
        ['id' => 1, 'title' => 'Test1'],
        ['id' => 2, 'title' => 'Test2'],
    ],
    'columns' => [
        'id',
        'title',
    ],
    'tableOptions' => [
        'class' => 'table table-striped table-bordered',
    ],
]) ?>

<?= \nullref\datatable\DataTable::widget([
    'serverSide' => true,
    'ajax' => '/site/datatables',
    'columns' => [
        'id',
        'title',
        'newTitle',
        'slug',
        [
            'class' => 'nullref\datatable\LinkColumn',
            'url' => ['/model/delete'],
            'options' => ['data-confirm' => 'Are you sure you want to delete this item?', 'data-method' => 'post'],
            'queryParams' => ['id'],
            'label' => 'Delete',
        ],
    ],
    'tableOptions' => [
        'class' => 'table table-striped table-bordered',
    ],
]) ?>
