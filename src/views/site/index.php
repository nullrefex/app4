<?php

use nullref\cms\components\Block;

/* @var $this yii\web\View */
$this->title = Yii::$app->name;
$this->registerJs(<<<JS
easyedit.setClickCallback('cms', function (block, e) {
console.log('You click on CMS block '+block.id);
})
JS
);
?>
<style>
    .site-index > * > .alert.alert-danger {
        background: green;
    }
</style>
<div class="site-index">
    <h1>Hello world</h1>
    <?= Block::getBlock('img')->run() ?>
    <?= Block::getBlock('editable1')->run() ?>
    <?= Block::getBlock('old_php')->run() ?>
    <?= Block::getBlock('123456 ')->run() ?>
    <?= Block::getBlock('editable2')->run() ?>
</div>
