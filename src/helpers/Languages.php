<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\helpers;


use nullref\core\components\LanguageManager;
use Yii;
use yii\helpers\ArrayHelper;

class Languages
{
    /**
     * @return array
     */
    public static function getMap()
    {
        return ArrayHelper::map(self::getManager()->getLanguages(), 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getSlugMap()
    {
        return ArrayHelper::map(self::getManager()->getLanguages(), 'id', 'slug');
    }

    /**
     * @return LanguageManager
     * @throws \yii\base\InvalidConfigException
     */
    public static function getManager()
    {
        return Yii::$app->get('languageManager');
    }

    /**
     * @return \nullref\core\interfaces\ILanguage
     */
    public static function getCurrent()
    {
        return self::getManager()->getCurrentLanguage();
    }

    /**
     * @param $langId
     * @return string
     */
    public static function getTitleById($langId)
    {
        $list = self::getManager()->getLanguages();

        if (isset($list[$langId])) {
            return $list[$langId]->getTitle();
        }
        return Yii::t('app', 'N\A');
    }
}