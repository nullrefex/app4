<?php

use yii\db\Migration;
use yii\db\Schema;

class m151022_004300_models_relation extends Migration
{
    use \nullref\core\traits\MigrationTrait;

    protected $tableName = '{{%product}}';

    public function up()
    {
        if (!$this->hasColumn($this->tableName, 'categoryId')) {
            $this->addColumn($this->tableName, 'categoryId', Schema::TYPE_INTEGER);
        }
    }

    public function down()
    {
        if ($this->hasColumn($this->tableName, 'categoryId')) {
            $this->dropColumn($this->tableName, 'categoryId');
        }
        return true;
    }

}


