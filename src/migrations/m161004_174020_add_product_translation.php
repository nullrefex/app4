<?php

use yii\db\Migration;

class m161004_174020_add_product_translation extends Migration
{
    public function up()
    {
        $this->createTable('{{%product_translation}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'language' => $this->integer(),
            'title' => $this->string(),
        ]);

        $this->dropColumn('{{%product}}', 'title');
    }

    public function down()
    {
        $this->addColumn('{{%product}}', 'title', $this->string());
        $this->dropTable('{{%product_translation}}');
    }
}
