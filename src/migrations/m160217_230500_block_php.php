<?php

use yii\db\Migration;
use yii\db\Schema;
use nullref\cms\models\Block;

class m160217_230500_block_php extends Migration
{
    public function up()
    {
        /** @var Block $existBlock */
        $existBlock = Block::findOne(['id' => 'php']);
        if( $existBlock ) {
            $oldId = 'old_' . $existBlock->id;
            $existBlock->id = $oldId;
            $existBlock->save();
        }
        $this->insert(Block::tableName(), [
			'id' => 'php',
			'class_name' => 'php',
			'name' => 'php',
			'visibility' => '1',
			'config' => serialize([
    "content" => "<h1 style=\"text-align: center;\">Додаток: \"<?= Yii::\$app->name ?>\"</h1>",
    "tag" => "div",
    "tagClass" => ""
    ]),
			'created_at' => 1455750311,
			'updated_at' => 1455750311,
			]);
    }

    public function down()
    {
        $this->delete(Block::tableName(), ['id' => 'php']);
        /** @var Block $oldBlock */
        $oldBlock = Block::findOne(['id' => 'old_php']);
        if( $oldBlock ) {
            $oldBlock->id = 'php';
            $oldBlock->save();
        }
        return true;
    }
}
