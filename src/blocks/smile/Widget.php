<?php
/**
 *
 */

namespace app\blocks\smile;

use nullref\cms\blocks\text\Widget as BaseWidget;
use yii\helpers\Html;


class Widget extends BaseWidget
{
    public function run()
    {
        return
            Html::beginTag($this->tag,['class'=>$this->tagClass]).
           '=) ' .Html::encode($this->content) . '^_^'.
            Html::endTag($this->tag);
    }
}