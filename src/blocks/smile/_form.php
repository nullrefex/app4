<?php
/**
 * @var $form \yii\widgets\ActiveForm
 * @var $block \nullref\cms\blocks\html\Block
 */

echo $form->field($block, 'content')->textarea();