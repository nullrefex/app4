<?php

namespace app\blocks\smile;

use nullref\cms\components\Block as BaseBlock;
/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $content;
    public $tag = 'div';
    public $tagClass = 'alert alert-success';

    public function getName()
    {
        return 'Smile HTML Block';
    }

    public function rules()
    {
        return [
            [['content'],'required'],
        ];
    }

}