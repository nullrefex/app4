<?php

namespace app\blocks\editable;

use nullref\cms\components\Block as BaseBlock;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $content;

    public function getName()
    {
        return 'Editable Block';
    }

    public function rules()
    {
        return [
            [['content'], 'required'],
        ];
    }
}