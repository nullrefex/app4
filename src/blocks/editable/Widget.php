<?php

namespace app\blocks\editable;

use app\components\easyedit\EasyEditWidget;
use nullref\cms\components\SetBlockWidget;
use nullref\cms\components\Widget as BaseWidget;


class Widget extends BaseWidget
{
    use SetBlockWidget;

    public $content;


    public function run()
    {
        return EasyEditWidget::widget(['content' => $this->block->content, 'contentId' => $this->block->id]);
    }
}