<?php


/**
 * @var $this \yii\web\View
 * @var $form \app\widgets\MultilingualForm
 * @var $model \yii\base\Model
 * @var $tab Closure
 */
use app\helpers\Languages;

?>


<div class="multilingual">
    <ul class="nav nav-tabs">
        <?php $first = true;
        foreach (Languages::getSlugMap() as $key => $lang): ?>
            <li class="<?= ($first) ? 'active' : '' ?>">
                <a data-toggle="tab" href="#<?= $lang ?>">
                    <?= Yii::t('translation', Languages::getMap()[$key]) ?>
                </a>
            </li>
            <?php $first = false; endforeach ?>
    </ul>
    <div class="tab-content">
        <?php $first = true;
        foreach (Languages::getSlugMap() as $key => $lang): ?>
            <div id="<?= $lang ?>" class="tab-pane fade <?= ($first) ? 'in active' : '' ?>">
                <?php $form->language = $lang ?>
                <?= call_user_func($tab, $form, $model) ?>
            </div>

            <?php $first = false; endforeach ?>
    </div>
</div>
