<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\modules\cms;

use app\components\CreateControllerTrait;

class Module extends \nullref\cms\Module
{
    use CreateControllerTrait;
}