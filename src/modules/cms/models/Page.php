<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\modules\cms\models;


use nullref\cms\models\Page as BasePage;
use Yii;

class Page extends BasePage
{
    public static function getMetaTypesList()
    {
        return array_merge(parent::getMetaTypesList(), [
            'my-custom-tag' => Yii::t('app', 'My Custom Tag'),
        ]);
    }

}