<?php

use app\widgets\Multilingual;
use yii\base\Model;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model nullref\product\models\Product */
/* @var $form yii\widgets\ActiveForm */
$categories = \nullref\category\models\Category::find()->asArray()->all();
$categories = \yii\helpers\ArrayHelper::map($categories, 'id', 'title');
?>

<style>
    .tab-content {
        padding: 15px;
        margin-bottom: 5px;
        border: 1px solid #ddd;
        border-top: none;
        border-radius: 5px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
</style>
<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <div class="row">
        <div class="col-md-12">
            <?= Multilingual::widget(['model' => $model,
                'tab' => function (ActiveForm $form, Model $model) {
                    echo $form->field($model, 'title')->textInput();
                }
            ]) ?>
        </div>
    </div>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'categoryId')->dropDownList($categories) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('product', 'Create') : Yii::t('product', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
