<?php

namespace app\controllers;

use app\models\Product;
use nullref\blog\models\Post;
use Yii;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\Controller;


class SiteController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionList()
    {
        return $this->render('list');
    }

    public function actions()
    {
        return [
            'datatables' => [
                'class' => 'nullref\datatable\DataTableAction',
                'query' => Post::find(),
                'applyOrder' => function (Query $query, $columns, $order) {
                    return $query->orderBy(['id' => ($order[0]['dir'] == 'asc') ? SORT_ASC : SORT_DESC]);
                },
                'applyFilter' => function (Query $query, $columns, $search) {
                    $query->select(['id', 'title', 'slug',]);
                    return $query;
                },
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

}
