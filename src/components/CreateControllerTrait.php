<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\components;

use Yii;

/**
 * Class CreateControllerTrait
 * @package app\components
 *
 */
trait CreateControllerTrait
{

    public function createController($route)
    {
        if ($route === '') {
            $route = $this->defaultRoute;
        }

        // double slashes or leading/ending slashes may cause substr problem
        $route = trim($route, '/');
        if (strpos($route, '//') !== false) {
            return false;
        }

        $id = $route;
        $route = '';
        while (($pos = strrpos($id, '/')) !== false) {
            if (isset($this->controllerMap[$id])) {
                $controller = Yii::createObject($this->controllerMap[$id], [$id, $this]);
                return [$controller, $route];
            }
            if ($route === '') {
                $route = substr($id, $pos + 1);
            } else {
                $route = substr($id, $pos + 1).'/'.$route;
            }
            $id = substr($id, 0, $pos);
        }

        // module and controller map take precedence
        if (isset($this->controllerMap[$id])) {
            $controller = Yii::createObject($this->controllerMap[$id], [$id, $this]);
            return [$controller, $route];
        }
        $module = $this->getModule($id);
        if ($module !== null) {
            return $module->createController($route);
        }

        if (($pos = strrpos($route, '/')) !== false) {
            $id .= '/' . substr($route, 0, $pos);
            $route = substr($route, $pos + 1);
        }

        $controller = $this->createControllerByID($id);
        if ($controller === null && $route !== '') {
            $controller = $this->createControllerByID($id . '/' . $route);
            $route = '';
        }

        return $controller === null ? false : [$controller, $route];
    }
}