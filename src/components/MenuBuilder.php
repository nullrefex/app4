<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2015 NRE
 */

namespace app\components;

use nullref\admin\components\MenuBuilder as BaseBuilder;


class MenuBuilder extends BaseBuilder
{
    /**
     * @param array $items
     * @return array
     */
    public function build($items)
    {
        $items['users']=[
            'label'=>'Users',
            'url'=>['/admin/user'],
            'icon'=>'users',
        ];
        return $items;
    }

}