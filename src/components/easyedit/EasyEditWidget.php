<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\components\easyedit;


use yii\base\Widget;

class EasyEditWidget extends Widget
{
    protected static $isCalled = false;
    public $content;
    public $contentId;

    public function run()
    {
        EasyEditAssets::register($this->view);
        $this->singleAction();
        $this->view->registerJs("easyedit.register('$this->contentId');");
        echo $this->startMarker();
        echo $this->content;
        echo $this->endMarker();
    }

    protected function singleAction()
    {
        if (self::$isCalled) {
            return;
        }
        self::$isCalled = true;
        return;
        $this->view->registerJs(<<<JS
easyedit.setClickCallback('openModal', function (block) {
    jQuery.ajax({
        url: '/cms/admin/block/config?id=' + block.id + '&redirect_to=' + window.location.pathname,
        success: function (result) {
            var modal = jQuery('#easyeditModal');
            modal.find('.modal-body').html(result);
            modal.modal('show');
        }
    });

});
JS
        );
        echo <<<HTML
<div class="modal fade" tabindex="-1" role="dialog" id="easyeditModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
HTML;

    }

    protected function startMarker()
    {
        return '<easy-edit contenteditable="true" id="' . $this->contentId . '">';
        return '<!-- easy-edit-start #' . $this->contentId . '-->';
    }

    protected function endMarker()
    {
        return '</easy-edit>';
        return '<!-- easy-edit-end #' . $this->contentId . '-->';
    }

}