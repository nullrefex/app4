(function () {
    function each(list, fn) {
        for (var key in list) {
            if (list.hasOwnProperty(key)) {
                fn(list[key], key);
            }
        }
    }

    var blocks = {};
    var setting = {};
    var comments = [];
    var callbacks = {
        click: {}
    };

    function initObject(id) {
        return {
            id: id,
            comments: {
                start: undefined,
                end: undefined
            }
        };
    }

    var self = {
        getComments: function () {
            return comments;
        },
        register: function (id) {
            blocks[id] = initObject(id);
        },
        setup: function (values) {
            each(values, function (value, key) {
                setting[key] = value;
            })
        },
        each: function (fn) {
            each(blocks, fn);
        },
        log: function () {
            console.log('Easy Edit blocks:');
            self.each(function (value, key) {
                console.log('- #' + key);
                console.log(value);
            });
        },
        setClickCallback: function (key, fn) {
            callbacks.click[key] = fn;
        }
    };

    var Type = {none: 0, start: 1, end: 2};

    function getCommentId(content) {
        var list = content.split(' ');
        if (list.length > 1) {
            return list[1].replace('#', '');
        }
        return null;
    }

    function parseComment(comment) {
        var content = comment.textContent.trim();
        var type = Type.none;
        if (content.indexOf('easy-edit-start') === 0) {
            type = Type.start;
        }
        if (content.indexOf('easy-edit-end') === 0) {
            type = Type.end;
        }
        return {
            element: comment,
            type: type,
            id: getCommentId(content)
        }
    }


    function filterNone() {
        return NodeFilter.FILTER_ACCEPT;
    }

    function getComments(rootElem) {
        var comments = [];
        // Fourth argument, which is actually obsolete according to the DOM4 standard, is required in IE 11
        var iterator = document.createNodeIterator(rootElem, NodeFilter.SHOW_COMMENT, filterNone, false);
        var curNode;
        while (curNode = iterator.nextNode()) {
            comments.push(curNode);
        }
        return comments;
    }

    function getWrapper(comments) {
        var wrappers = [];
        each(comments, function (node) {
            var parent = node.parentElement;
            if (wrappers.indexOf(parent) > -1) {
                return;
            }
            wrappers.push(parent);
        });
        return wrappers;
    }

    window.addEventListener('load', function () {
        var comments = getComments(document.body);
        each(comments, function (comment) {
            var data = parseComment(comment);
            if (blocks[data.id]) {
                var block = blocks[data.id];
                switch (data.type) {
                    case Type.start:
                        block.comments.start = data;
                        break;
                    case Type.end:
                        block.comments.end = data;
                        break;
                    default:
                        break;
                }
            }
        });
        each(blocks, function (block) {
            if (block.comments.start.element.nextSibling === block.comments.end.element.previousSibling) {
                block.element = block.comments.start.element.nextSibling;
            }
        });

        var wrappers = getWrapper(comments);
        each(wrappers, wrapperHandler);
    });

    function findAsParent(element, node) {
        if (node === null) {
            return Infinity;
        }
        if (node.parentElement === element) {
            return 1;
        }
        return 1 + findAsParent(element, node.parentElement);
    }

    function checkIfEditable(node, e) {
        var resultBlock;
        each(blocks, function (block) {
            var min = Infinity;
                if (block.element === node) {
                resultBlock = block;
            } else {
                var result = findAsParent(block.element, node);
                if (result < min) {
                    resultBlock = block;
                    min = result;
                }
            }
        });
        if (resultBlock) {
            console.log(resultBlock);
            each(callbacks.click, function (fn, key) {
                fn(resultBlock, e);
            })
        }
    }

    function wrapperHandler(el) {
return;
        el.addEventListener("click", function (e) {
            var selection = document.getSelection(),
                node = selection.baseNode;

            selection.removeAllRanges();
            checkIfEditable(node, e);
        })
    }

    window.easyedit = self;
})();