<?php

namespace app\components\easyedit;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */
class EasyEditAssets extends AssetBundle
{
    public $sourcePath = '@app/components/easyedit/assets';

    public $js = [
        'easyedit.js',
    ];
}
