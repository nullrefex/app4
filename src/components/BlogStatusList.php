<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2015 NRE
 */

namespace app\components;


class BlogStatusList extends \nullref\blog\components\BlogStatusList
{
    const STATUS_HIDE = 3;

    public function getList()
    {
        return array_merge(parent::getList(),[
            \Yii::t('app','Hide'),
        ]);
    }
}