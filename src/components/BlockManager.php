<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2015 NRE
 */

namespace app\components;


class BlockManager extends \nullref\cms\components\BlockManager
{
    public function getList()
    {
        return array_merge([
            'smile' => 'app\blocks\smile',
            'editable' => 'app\blocks\editable',
        ], parent::getList());
    }
}