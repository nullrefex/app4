<?php

namespace app\models;

use app\helpers\Languages;
use nullref\useful\behaviors\TranslationBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product_v2".
 *
 * @property integer $id
 * @property string $title
 * @property string $price
 *
 *
 * @property ProductTranslation[] $translations
 */
class Product extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    public function behaviors()
    {
        return [
            'multilanguage' => [
                'class' => TranslationBehavior::className(),
                'languages' => Languages::getSlugMap(),
                'defaultLanguage' => Languages::getCurrent()->getSlug(),
                'relation' => 'translations',
                'attributeNamePattern' => '{attr}_{lang}',
                'languageField' => 'language',
                'langClassName' => ProductTranslation::className(),
                'translationAttributes' => [
                    'title',
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'title'], 'required'],
            [['price'], 'number'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductTranslation::className(), ['product_id' => 'id']);
    }

}
