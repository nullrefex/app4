<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2015 NRE
 */

namespace app\models;


use nullref\blog\models\PostQuery;

class BlogPostQuery extends PostQuery
{
    /**
     * @inheritdoc
     * @return Post[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Post|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}