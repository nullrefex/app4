<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2015 NRE
 */

namespace app\models;


use nullref\blog\models\Post;

class BlogPost extends Post
{
    public function getNewTitle()
    {
        return 'New:' . $this->title;
    }

    public function fields()
    {
        return array_merge(parent::fields(), ['newTitle']);
    }

    public function init()
    {
        parent::init();
    }

}