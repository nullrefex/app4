run:
	cd web && open http://localhost:8090 && php -S localhost:8090
update-vendors:
	php composer.phar update
install-vendors:
	php composer.phar install
get-composer:
	curl -sS https://getcomposer.org/installer | php
db-migrate:
	php yii modules-migrate
